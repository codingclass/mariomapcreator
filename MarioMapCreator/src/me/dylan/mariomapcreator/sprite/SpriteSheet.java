package me.dylan.mariomapcreator.sprite;

import java.awt.image.BufferedImage;

public class SpriteSheet {
	
	private BufferedImage[][] sprites = null;
	private int spritesx;
	private int spritesy;
	private BufferedImage image;
	
	public SpriteSheet(BufferedImage image, int width, int height, int border) {
		spritesx = (image.getWidth() - (border * width - 1)) / width + 1;
		spritesy = (image.getHeight() - (border * height - 1)) / height + 1;
		System.out.println(spritesx + " | " + spritesy);
		this.image = image;
		sprites = new BufferedImage[spritesx][spritesy];
		for (int x = 0; x < spritesx; x ++) {
			for (int y = 0; y < spritesy; y ++) {
				sprites[x][y] = image.getSubimage(x * 16 + (x == 0 ? 1 : x) * border - (x == 0 ? 1 : 0), y * 16 + (y == 0 ? 1 : y) * border - (y == 0 ? 1 : 0), width, height);
			}
		}
		System.out.println("Loaded a spritesheet with " + spritesx * spritesy + " sprites!");
	}
	
	public BufferedImage getSprite(int x, int y) {
		return sprites[x][y];
	}
	
	public BufferedImage getSprite(int i) {
		int x = i % spritesx;
		int y = Math.floorDiv(i, spritesx);
		return sprites[x][y];
	}
	
	public int getSprite2(int x, int y) {
		BufferedImage[] sprites = getSprites2();
		return (spritesx * y) + x;
	}
	
	public BufferedImage[][] getSprites() {
		return sprites;
	}
	
	public BufferedImage[] getSprites2() {
		BufferedImage[] sprites = new BufferedImage[spritesx * spritesy];
		for (int x = 0; x < spritesx; x ++) {
			for (int y = 0; y < spritesy; y ++) {
				sprites[x + y] = getSprite(x, y);
			}
		}
		return sprites;
	}
	
	public int getSpritesX() {
		return spritesx;
	}
	
	public int getSpritesY() {
		return spritesy;
	}
	
	public BufferedImage getSheet() {
		return image;
	}
	
	public BufferedImage getSpriteByTileLoc() {
		return null;
	}
	
}
