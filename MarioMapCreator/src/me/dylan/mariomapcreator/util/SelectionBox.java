package me.dylan.mariomapcreator.util;

public class SelectionBox {
	
	int xInTiles;
	int yInTiles;
	int widthInTiles;
	int heightInTiles;
	boolean visible = false;
	boolean isDrawing = false;
	boolean isMoving = false;
	
	public boolean isMoving() {
		return isMoving;
	}

	public void setMoving(boolean isMoving) {
		this.isMoving = isMoving;
	}

	public SelectionBox(int xInTiles, int yInTiles, int widthInTiles, int heightInTiles) {
		this.xInTiles = xInTiles;
		this.yInTiles = yInTiles;
		this.widthInTiles = widthInTiles;
		this.heightInTiles = heightInTiles;
	}
	
	public boolean isDrawing() {
		return isDrawing;
	}

	public void setDrawing(boolean isDrawing) {
		this.isDrawing = isDrawing;
	}

	public int getX() {
		return xInTiles;
	}

	public void setxInTiles(int xInTiles) {
		this.xInTiles = xInTiles;
	}

	public int getY() {
		return yInTiles;
	}

	public void setyInTiles(int yInTiles) {
		this.yInTiles = yInTiles;
	}

	public int getWidth() {
		return widthInTiles;
	}

	public void setWidthInTiles(int widthInTiles) {
		this.widthInTiles = widthInTiles;
	}

	public int getHeight() {
		return heightInTiles;
	}

	public void setHeightInTiles(int heightInTiles) {
		this.heightInTiles = heightInTiles;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
}
