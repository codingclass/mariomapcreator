package me.dylan.mariomapcreator.util;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class ComboBoxRenderer extends JLabel implements ListCellRenderer<Object> {
	
    private boolean colorSet;
    private Color selectionBackgroundColor;

    public ComboBoxRenderer() {
        setOpaque(true);
        setHorizontalAlignment(LEFT);
        setVerticalAlignment(CENTER);
        colorSet = false;
        selectionBackgroundColor = Color.white;
    }

    @SuppressWarnings("rawtypes")
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        if(!colorSet) {
            selectionBackgroundColor = list.getSelectionBackground();
            colorSet = true;
        }
        
        list.setSelectionBackground(Color.white);

        if(isSelected)
            setBackground(selectionBackgroundColor);
        else
            setBackground(Color.white);

        setText((String)value);
        setFont(list.getFont());

        return this;
    }
}