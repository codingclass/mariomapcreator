package me.dylan.mariomapcreator.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import me.dylan.mariomapcreator.sprite.SpriteSheet;

public class AssetPanel extends JPanel {
	
	private final SpriteSheet tileSheet;
	private MapCreatorPanel mcp;
	private boolean isTileEditor = true;
	
	public AssetPanel(MapCreatorPanel mcp) {
		this.setBackground(Color.black);
		BufferedImage tiles = null;
		try {
			tiles = ImageIO.read(this.getClass().getResource("/SMB-Tiles.gif"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		tileSheet = new SpriteSheet(tiles, 16, 16, 1);
		this.setPreferredSize(new Dimension(tileSheet.getSheet().getWidth() * 5, tileSheet.getSheet().getHeight() * 5));
		this.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mousePressed(MouseEvent e) {
				super.mouseClicked(e);
				if (isTileEditor) {
					int xTile = Math.floorDiv(e.getX(), 16 * 5);
					int yTile = Math.floorDiv(e.getY(), 16 * 5);
					mcp.setCurrentTile(tileSheet.getSprite2(xTile, yTile));
					System.out.println("Selected tile: " + xTile + ", " + yTile);
				}
			}
			
		});

	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (this.isTileEditor) {
			Graphics2D g2d = (Graphics2D) g.create();
			//g2d.drawImage(tileSheet.getSprite(3, 0), 0, 0, 80, 80, null);
			BufferedImage[][] tiles = tileSheet.getSprites();
			int i = 0;
			for (int x = 0; x < tileSheet.getSpritesX(); x ++) {
				for (int y = 0; y < tileSheet.getSpritesY(); y ++) {
					g2d.drawImage(tiles[x][y], (16 * x) * 5, (16 * y) * 5, 80, 80, null);
					g2d.drawString("" + i, x * 80, y * 80);
					i ++;
				}
			}
			g2d.dispose();
		}
		/*Graphics2D g2d = (Graphics2D) g.create();
		g.drawImage(tileSheet.getSheet(), 0, 0, tileSheet.getSheet().getWidth() * 5, tileSheet.getSheet().getHeight() * 5, null);
		g2d.dispose();*/
	}
	
	public SpriteSheet getTileSheet() {
		return tileSheet;
	}
	
	public boolean isTileEditor() {
		return isTileEditor;
	}
	
	public void setTileEditor() {
		this.isTileEditor = true;
		repaint();
	}
	
	public void setObjectEditor() {
		this.isTileEditor = false;
		repaint();
	}
	
}
