package me.dylan.mariomapcreator.gui;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import me.dylan.mariomapcreator.map.Map;
import me.dylan.mariomapcreator.map.Map.MapLayer;
import me.dylan.mariomapcreator.map.TileLog;
import me.dylan.mariomapcreator.sprite.SpriteSheet;
import me.dylan.mariomapcreator.util.SelectionBox;

public class MapPanel extends JPanel {
	
	private final MapCreatorPanel mcp;
	private boolean isInPanel;
	private int mousex;
	private int mousey;
	private SpriteSheet tileSheet;
	private boolean grid = false;
	private int startx;
	private int starty;
	private Set<TileLog> tileLogs = new HashSet<TileLog>();
	private int[][] tileSelection;
	private SelectionBox selectionBox;
	private Thread runnable;
	private SelectionBox clipboard = null;
	private boolean moveCopy = false;
	private Map selectedMap;
	private int layerAlpha = 100;
	
	/*
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	
	public MapPanel(MapCreatorPanel mcp, File mapFile) {
		this.mcp = mcp;
		tileSheet = mcp.getTilePanel().getTileSheet();
		selectionBox = new SelectionBox(0, 0, 0, 0); //create the blank selection box
		selectedMap = new Map(mcp, this, mapFile, 400, 50, tileSheet.getSprite2(12, 6));
		this.setPreferredSize(new Dimension(selectedMap.getTileWidth() * 16, selectedMap.getTileHeight() * 16));
		this.addKeyListener(new KeyAdapter() {
			
			private Set<Integer> keysDown = new HashSet<Integer>();
			
			@Override
			public void keyPressed(KeyEvent e) {
				super.keyPressed(e);
				if (!keysDown.contains(e.getKeyCode())) {
					keysDown.add(e.getKeyCode());
				}
				if (keysDown.contains(17) && e.getKeyCode() == 67) { //Ctrl C
					if (selectionBox.isVisible())
						clipboard = selectionBox;
					System.out.println("Copied!");
				}
				if (keysDown.contains(17) && e.getKeyCode() == 86) { //Ctrl V
					if (clipboard != null && !selectionBox.isDrawing()) {
						selectionBox = clipboard;
						tileSelection = new int[selectionBox.getWidth()][selectionBox.getHeight()];
						for (int x = selectionBox.getX(); x < selectionBox.getX() + selectionBox.getWidth(); x ++) { //calculate selected tiles and save them
							for (int y = selectionBox.getY(); y < selectionBox.getY() + selectionBox.getHeight(); y ++) {
								if (selectedMap.getCurrentLayer() == MapLayer.FOREGROUND) {
									tileSelection[x - selectionBox.getX()][y - selectionBox.getY()] = selectedMap.getForeground()[x][y];
								} else {
									tileSelection[x - selectionBox.getX()][y - selectionBox.getY()] = selectedMap.getBackground()[x][y];
								}
							}
						}
						selectionBox.setMoving(true);
						selectionBox.setVisible(false);
						moveCopy = true;
						repaint();
					}
				}
				if (keysDown.contains(17) && e.getKeyCode() == 127) { //Ctrl Delete
					int choice = JOptionPane.showConfirmDialog(null, "Are you sure you want to restart the level?", "Clear level", 0);
					if (choice == 0) {
						for (int x = 0; x < selectedMap.getTileWidth(); x ++) {
							for (int y = 0; y < selectedMap.getTileHeight(); y ++) {
								selectedMap.getForeground()[x][y] = selectedMap.getBackgroundTile();
								selectedMap.getBackground()[x][y] = selectedMap.getBackgroundTile();
							}
						}
					}
				}
				if (keysDown.contains(17) && e.getKeyCode() == 78) { //Ctrl N
					mcp.newMap(null);
				}
				if (keysDown.contains(17) && e.getKeyCode() == 83) { //Ctrl S
					selectedMap.save();
				}
				if (keysDown.contains(79) && e.getKeyCode() == 79) { //Ctrl O
					mcp.openMap();
				}
				System.out.println(e.getKeyCode());
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				super.keyReleased(e);
				keysDown.remove(keysDown.remove(e.getKeyCode()));
			}
			
		});
		this.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mousePressed(MouseEvent e) {
				super.mousePressed(e);
				if (moveCopy) return;
				if (SwingUtilities.isRightMouseButton(e)) { //right click
					if (selectedMap.getCurrentLayer() == MapLayer.FOREGROUND) {
						mcp.setCurrentTile(selectedMap.getForeground()[Math.floorDiv(e.getX(), 16)][Math.floorDiv(e.getY(), 16)]);
					} else {
						mcp.setCurrentTile(selectedMap.getBackground()[Math.floorDiv(e.getX(), 16)][Math.floorDiv(e.getY(), 16)]);
					}
				} else if (SwingUtilities.isMiddleMouseButton(e)) { //middle click
					System.out.println("Flood fill!");
					int xorg = Math.floorDiv(e.getX(), 16);
					int yorg = Math.floorDiv(e.getY(), 16);
					if (selectedMap.getCurrentLayer() == MapLayer.FOREGROUND) {
						floodFill(xorg, yorg, selectedMap.getForeground()[Math.floorDiv(e.getX(), 16)][Math.floorDiv(e.getY(), 16)], mcp.getCurrentTile());
					} else {
						floodFill(xorg, yorg, selectedMap.getBackground()[Math.floorDiv(e.getX(), 16)][Math.floorDiv(e.getY(), 16)], mcp.getCurrentTile());
					}
				} else { //left click
					if (e.isControlDown()) { //if control is down
						selectionBox.setDrawing(true);
						startx = e.getX();
						starty = e.getY();
					} else { //if control is not down
						if (selectionBox.isVisible()) { //if there is a selection on the screen
							if (SwingUtilities.isRectangleContainingRectangle(new Rectangle(selectionBox.getX() * 16, selectionBox.getY() * 16,
									selectionBox.getWidth() * 16, selectionBox.getHeight() * 16), new Rectangle(mousex, mousey, 1, 1))) { //if click is in the selection
								System.out.println("Selection Movement: X: " + selectionBox.getX() + " | Y: " + selectionBox.getY() + " | W: " + selectionBox.getWidth() + " | H: " + selectionBox.getHeight());
								tileSelection = new int[selectionBox.getWidth()][selectionBox.getHeight()];
								for (int x = selectionBox.getX(); x < selectionBox.getX() + selectionBox.getWidth(); x ++) { //calculate selected tiles and save them
									for (int y = selectionBox.getY(); y < selectionBox.getY() + selectionBox.getHeight(); y ++) {
										if (selectedMap.getCurrentLayer() == MapLayer.FOREGROUND) {
											tileSelection[x - selectionBox.getX()][y - selectionBox.getY()] = selectedMap.getForeground()[x][y];
										} else {
											tileSelection[x - selectionBox.getX()][y - selectionBox.getY()] = selectedMap.getBackground()[x][y];
										}
									}
								}
								selectionBox.setMoving(true);
								for (int x = 0; x < selectionBox.getWidth(); x ++) {
									for (int y = 0; y < selectionBox.getHeight(); y++) {
										if (selectedMap.getCurrentLayer() == MapLayer.FOREGROUND) {
											selectedMap.getForeground()[x + selectionBox.getX()][y + selectionBox.getY()] = tileSheet.getSprite2(12, 6);
										} else {
											selectedMap.getBackground()[x + selectionBox.getX()][y + selectionBox.getY()] = tileSheet.getSprite2(12, 6);
										}
									}
								}
							}
							selectionBox.setVisible(false); //remove stationary box since we are moving it
						} else { //if no selection is on the screen
							tileLogs.clear();
							//tileLogs.add(new TileLog(Math.floorDiv(e.getX(), 16), Math.floorDiv(e.getY(), 16), tiles[Math.floorDiv(e.getX(), 16)][Math.floorDiv(e.getY(), 16)], mcp.getCurrentTile()));
							if (selectedMap.getCurrentLayer() == MapLayer.FOREGROUND) {
								selectedMap.getForeground()[Math.floorDiv(e.getX(), 16)][Math.floorDiv(e.getY(), 16)] = mcp.getCurrentTile();
							} else {
								selectedMap.getBackground()[Math.floorDiv(e.getX(), 16)][Math.floorDiv(e.getY(), 16)] = mcp.getCurrentTile();
							}
						}
					}
				}
				repaint();
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				super.mouseEntered(e);
				isInPanel = true;
				if (runnable == null) {
					runnable = new Thread(new Runnable() { //loop while the mouse is inside of the panel
						
						public void run() {
							SpriteSheet tileSheet = mcp.getTilePanel().getTileSheet();
							requestFocus();
							while(isInPanel) {
								Graphics2D g2d = (Graphics2D) getGraphics().create();
								repaint();
								if (selectionBox.isDrawing() == false) { //if not making a selection
									g2d.drawImage(tileSheet.getSprite(mcp.getCurrentTile()), mousex - 8, mousey - 8, 16, 16, null);
								} else { //if is making a selection
									//DRAW THE SELECTION CREATION BOX
									g2d.setColor(Color.darkGray);
									int newx = startx;
									int newy = starty;
									int neww = mousex - startx;
									int newh = mousey - starty;
									if (mousex - startx < 0) {
										newx = mousex;
										neww = startx - mousex;
									}
									if (mousey - starty < 0) {
										newy = mousey;
										newh = starty - mousey;
									}
									g2d.drawRect(newx, newy, neww, newh);
								}
								if (selectionBox.isMoving()) { //if the user is moving the box
									g2d.setColor(Color.darkGray);
									int newx = selectionBox.getX() * 16 + (mousex - selectionBox.getX() * 16);
									int newy = selectionBox.getY() * 16 + (mousey - selectionBox.getY() * 16);
									if (mousex - selectionBox.getX() * 16 < 0) {
										selectionBox.setxInTiles(selectionBox.getX() - (selectionBox.getX() - Math.floorDiv(mousex, 16)));
									}
									if (mousey - selectionBox.getY() * 16 < 0) {
										selectionBox.setyInTiles(selectionBox.getY() - (selectionBox.getY() - Math.floorDiv(mousey, 16)));
									}
									//DRAW SELECTED TILES WHILE MOVING
									for (int x = 0; x < selectionBox.getWidth(); x ++) {
										for (int y = 0; y < selectionBox.getHeight(); y ++) {
											if (tileSelection[x][y] == tileSheet.getSprite2(12, 6))
												continue;
											g2d.drawImage(tileSheet.getSprite(tileSelection[x][y]), newx + x * 16, newy + y * 16, 16, 16, null);
										}
									}
									g2d.drawRect(newx, newy, selectionBox.getWidth() * 16, selectionBox.getHeight() * 16);
									if (moveCopy) {
										selectionBox.setxInTiles(Math.floorDiv(mousex, 16));
										selectionBox.setyInTiles(Math.floorDiv(mousey, 16));
									}
								}
								g2d.dispose();
							}
						}
						
					});
					runnable.start();
				}
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				super.mouseExited(e);
				isInPanel = false;
				runnable = null;
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				if (selectionBox.isDrawing()) {
					selectionBox.setxInTiles(Math.floorDiv(startx, 16));
					selectionBox.setyInTiles(Math.floorDiv(starty, 16));
					selectionBox.setWidthInTiles(Math.floorDiv(mousex, 16) - selectionBox.getX());
					selectionBox.setHeightInTiles(Math.floorDiv(mousey, 16) - selectionBox.getY());
					if (mousex - startx < 0) {
						selectionBox.setxInTiles(Math.floorDiv(mousex, 16));
						selectionBox.setWidthInTiles(Math.floorDiv(startx, 16) - selectionBox.getX());
					}
					if (mousey - starty < 0) {
						selectionBox.setyInTiles(Math.floorDiv(mousey, 16));
						selectionBox.setHeightInTiles(Math.floorDiv(starty, 16) - selectionBox.getY());
					}
					selectionBox.setDrawing(false);
					selectionBox.setVisible(true);
					repaint();
				}
				if (selectionBox.isMoving()) {
					for (int x = 0; x < selectionBox.getWidth(); x ++) {
						for (int y = 0; y < selectionBox.getHeight(); y ++) {
							if (tileSelection[x][y] != tileSheet.getSprite2(12, 6)) {
								if (selectedMap.getCurrentLayer() == MapLayer.FOREGROUND) {
									selectedMap.getForeground()[x + selectionBox.getX()][y + selectionBox.getY()] = tileSelection[x][y];
								} else {
									selectedMap.getBackground()[x + selectionBox.getX()][y + selectionBox.getY()] = tileSelection[x][y];
								}
							}
						}
					}
					if (moveCopy)
						moveCopy = false;
					selectionBox.setMoving(false);
					selectionBox.setVisible(true);
					repaint();
				}
				super.mouseReleased(e);
			}
			
		});
		
		this.addMouseMotionListener(new MouseAdapter() {
			
			@Override
			public void mouseMoved(MouseEvent e) {
				super.mouseMoved(e);
				mousex = e.getX();
				mousey = e.getY();
			}
			
			@Override
			public void mouseDragged(MouseEvent e) {
				super.mouseDragged(e);
				mousex = e.getX();
				mousey = e.getY();
				if (SwingUtilities.isLeftMouseButton(e)) {
					if (!e.isControlDown()) {
						if (!selectionBox.isMoving()) {
							if (selectedMap.getCurrentLayer() == MapLayer.FOREGROUND) {
								//tileLogs.add(new TileLog(Math.floorDiv(e.getX(), 16), Math.floorDiv(e.getY(), 16), tiles[Math.floorDiv(e.getX(), 16)][Math.floorDiv(e.getY(), 16)], mcp.getCurrentTile()));
								selectedMap.getForeground()[Math.floorDiv(e.getX(), 16)][Math.floorDiv(e.getY(), 16)] = mcp.getCurrentTile();
							} else {
								selectedMap.getBackground()[Math.floorDiv(e.getX(), 16)][Math.floorDiv(e.getY(), 16)] = mcp.getCurrentTile();
							}
						} else {
							if (SwingUtilities.isRectangleContainingRectangle(new Rectangle(selectionBox.getX() * 16, selectionBox.getY() * 16, selectionBox.getWidth() * 16, selectionBox.getHeight() * 16), new Rectangle(mousex, mousey, 1, 1))) {
								selectionBox.setxInTiles(Math.floorDiv(mousex, 16));
								selectionBox.setyInTiles(Math.floorDiv(mousey, 16));
							}
						}
					}
				}
			}
			
		});
		
	}
	
	@Override
	protected void paintComponent(Graphics g) { //ADD ALPHA CHANNELS FOR LAYERS
		super.paintComponent(g);
		g.setColor(Color.gray);
		Graphics2D g2d = (Graphics2D) g.create();
		for (int x = 0; x < selectedMap.getTileWidth(); x ++) {
			for (int y = 0; y < selectedMap.getTileHeight(); y ++) {
				if (selectedMap.getCurrentLayer() == MapLayer.FOREGROUND) {
					if (selectedMap.getBackground()[x][y] != selectedMap.getBackgroundTile())
						g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) layerAlpha / 100));
					else
						g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1));
				} else
					g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1));
				g2d.drawImage(tileSheet.getSprite(selectedMap.getBackground()[x][y]), x * 16, y * 16, null);
				if (selectedMap.getForeground()[x][y] != selectedMap.getBackgroundTile()) {
					if (selectedMap.getCurrentLayer() == MapLayer.BACKGROUND)
						g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) layerAlpha / 100));
					else
						g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) 1));
					g2d.drawImage(tileSheet.getSprite(selectedMap.getForeground()[x][y]), x * 16, y * 16, null);
				}
			}
		}
		g2d.dispose();
		if (grid) {
			for (int x = 0; x < this.getWidth(); x += 16) {
				g.drawLine(x, 0, x, this.getHeight());
			}
			for (int y = 0; y < this.getHeight(); y += 16) {
				g.drawLine(0, y, this.getWidth(), y);
			}
		}
		if (selectionBox.isVisible()) {
			g.setColor(Color.darkGray);
			g.drawRect(selectionBox.getX() * 16, selectionBox.getY() * 16, selectionBox.getWidth() * 16, selectionBox.getHeight() * 16);
		}
	}
	
	public void toggleGrid() {
		this.grid = !grid;
		repaint();
	}
	
	private void floodFill(int x, int y, int tile, int ntile) {
		if (selectedMap.getCurrentLayer() == MapLayer.FOREGROUND) {
			if (selectedMap.getForeground()[x][y] == tile) {
				selectedMap.getForeground()[x][y] = ntile;
				if (x - 1 >= 0)
					floodFill(x - 1, y, tile, ntile);
				if (x + 1 < this.getWidth() / 16)
					floodFill(x + 1, y, tile, ntile);
				if (y - 1 >= 0)
					floodFill(x, y - 1, tile, ntile);
				if (y + 1 < this.getHeight() / 16)
					floodFill(x, y + 1, tile, ntile);
			}
		} else {
			if (selectedMap.getBackground()[x][y] == tile) {
				selectedMap.getBackground()[x][y] = ntile;
				if (x - 1 >= 0)
					floodFill(x - 1, y, tile, ntile);
				if (x + 1 < this.getWidth() / 16)
					floodFill(x + 1, y, tile, ntile);
				if (y - 1 >= 0)
					floodFill(x, y - 1, tile, ntile);
				if (y + 1 < this.getHeight() / 16)
					floodFill(x, y + 1, tile, ntile);
			}
		}
	}
	
	public void undo() {
		/*if (tileLogs.isEmpty())
			return;
		for (TileLog log : tileLogs) {
			tiles[log.getX()][log.getY()] = log.getLastTile();
		}*/
		tileLogs.clear();
	}
	
	public void changeAlpha(int alpha) {
		this.layerAlpha = alpha;
		repaint();
	}
	
	public Map getMap() {
		return selectedMap;
	}
	
}
