package me.dylan.mariomapcreator.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import me.dylan.mariomapcreator.MapCreator;
import me.dylan.mariomapcreator.map.Map.MapLayer;
import me.dylan.mariomapcreator.script.ScriptManager;
import me.dylan.mariomapcreator.util.ComboBoxRenderer;

public class MapCreatorPanel extends JPanel {
	
	private int currentTile = 0;
	private AssetPanel assetPanel;
	private List<MapPanel> mapPanels = new ArrayList<MapPanel>();
	private ImageIcon levelIcon = null;
	private ImageIcon flagIcon = null;
	private ImageIcon scriptIcon = null;
	private JTabbedPane mapTabPanel;
	int mapIndex = 0;
	private ScriptManager scriptManager;
	
	public MapCreatorPanel() {
		this.setLayout(null);
		levelIcon = new ImageIcon(MapCreator.class.getResource("/levelicon.png"));
		flagIcon = new ImageIcon(MapCreator.class.getResource("/flagicon.png"));
		scriptIcon = new ImageIcon(MapCreator.class.getResource("/scripticon.png"));
		//scriptManager = new ScriptManager();
	}
	
	public void init() {
		assetPanel = new AssetPanel(this);
		assetPanel.setBorder(BorderFactory.createLineBorder(Color.black, 1));
		JScrollPane assetScroller = new JScrollPane(assetPanel);
		assetScroller.setSize(300, this.getHeight() - 200);
		assetScroller.setLocation(this.getWidth() - 325, 100);
		assetScroller.getVerticalScrollBar().setUnitIncrement(20);
		
		mapTabPanel = new JTabbedPane();
		mapTabPanel.setSize(this.getWidth() - 400, this.getHeight() - 180);
		mapTabPanel.setLocation(50, 80);
		
		newMap(null);
		
		JButton newb = new JButton("New");
		newb.setToolTipText("Create a new map");
		newb.setSize(100, 20);
		newb.setLocation(25, 10);
		newb.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				newMap(null);
			}
			
		});
		
		JButton close = new JButton("Close Map");
		close.setToolTipText("Close a map tab");
		close.setSize(100, 20);
		close.setLocation(150, 10);
		close.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (mapPanels.size() > 1) {
					MapPanel panel = mapPanels.get(mapTabPanel.getSelectedIndex());
					mapPanels.remove(panel);
					mapTabPanel.remove(mapTabPanel.getSelectedIndex());
				}
			}
			
		});
		
		JButton open = new JButton("Open");
		open.setToolTipText("Open a Mario map");
		open.setSize(100, 20);
		open.setLocation(this.getWidth() - 250, 10);
		open.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				openMap();
			}
			
		});
		
		JButton save = new JButton("Save");
		save.setToolTipText("Save the Mario map");
		save.setSize(100, 20);
		save.setLocation(this.getWidth() - 125, 10);
		save.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				MapPanel currMapPanel = mapPanels.get(mapTabPanel.getSelectedIndex());
				currMapPanel.getMap().save();
			}
			
		});
		
		JButton grid = new JButton("Grid");
		grid.setToolTipText("Toggles grid for map creator");
		grid.setSize(75, 20);
		grid.setLocation(mapTabPanel.getX(), mapTabPanel.getY() + mapTabPanel.getHeight());
		grid.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mapPanels.get(mapTabPanel.getSelectedIndex()).toggleGrid();
			}
			
		});
		
		JButton undo = new JButton("Undo");
		undo.setToolTipText("Undoes the last action for map creator");
		undo.setSize(75, 20);
		undo.setLocation(mapTabPanel.getX() + 75, mapTabPanel.getY() + mapTabPanel.getHeight());
		undo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				//mapPanel.undo();
			}
			
		});
		
		String[] assetChoices = {"Tile Editor", "Object Editor"};
		
		JComboBox<String> assetChoice = new JComboBox<String>(assetChoices);
		assetChoice.setBackground(Color.white);
		assetChoice.setSize(300, 20);
		assetChoice.setLocation(assetScroller.getX(), 80);
		assetChoice.setRenderer(new ComboBoxRenderer());
		assetChoice.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if (assetChoice.getSelectedItem().equals(assetChoices[0])) {
					assetPanel.setTileEditor();
				} else {
					assetPanel.setObjectEditor();
				}
			}
			
		});
		
		String[] fbo = {"Foreground", "Background"};
		
		JComboBox<String> layerChoice = new JComboBox<String>(fbo);
		layerChoice.setBackground(Color.white);
		layerChoice.setSize(150, 20);
		layerChoice.setLocation(mapTabPanel.getX() + mapTabPanel.getWidth() - 150, mapTabPanel.getY() + mapTabPanel.getHeight());
		layerChoice.setRenderer(new ComboBoxRenderer());
		layerChoice.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if (layerChoice.getSelectedItem().equals("Foreground")) {
					for (MapPanel panel : mapPanels)
						panel.getMap().setCurrentLayer(MapLayer.FOREGROUND);
				} else if (layerChoice.getSelectedItem().equals("Background")) {
					for (MapPanel panel : mapPanels)
						panel.getMap().setCurrentLayer(MapLayer.BACKGROUND);
				}
			}
			
		});
		
		JSlider alphaSlider = new JSlider(JSlider.HORIZONTAL, 0, 100, 100);
		alphaSlider.setMinorTickSpacing(1);
		alphaSlider.setMajorTickSpacing(10);
		alphaSlider.setLocation(layerChoice.getX(), 60);
		alphaSlider.setSize(150, 20);
		alphaSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				for (MapPanel panel : mapPanels)
					panel.changeAlpha(alphaSlider.getValue());
			}
			
		});
		
		JButton flags = new JButton(flagIcon);
		flags.setSize(32, 32);
		flags.setLocation(assetScroller.getX(), 10);
		flags.setToolTipText("Edit level flags");
		
		JButton scripts = new JButton(scriptIcon);
		scripts.setSize(32, 32);
		scripts.setLocation(assetScroller.getX() + flags.getWidth() + 5, 10);
		scripts.setToolTipText("Edit map scripts and create new ones");
		
		this.add(mapTabPanel);
		this.add(assetScroller);
		this.add(newb);
		this.add(close);
		this.add(open);
		this.add(save);
		this.add(grid);
		this.add(undo);
		this.add(assetChoice);
		this.add(layerChoice);
		this.add(alphaSlider);
		this.add(flags);
		this.add(scripts);
	}
	
	public void setCurrentTile(int currentTile) {
		this.currentTile = currentTile;
	}
	
	public int getCurrentTile() {
		return currentTile;
	}
	
	public AssetPanel getTilePanel() {
		return assetPanel;
	}
	
	public MapPanel newMap(File file) {
		MapPanel mapPanel = null;
		if (file == null)
			mapPanel = new MapPanel(MapCreatorPanel.this, new File("newMap" + (mapIndex + 1)));
		else
			mapPanel = new MapPanel(MapCreatorPanel.this, file);
		mapPanel.setBorder(BorderFactory.createLineBorder(Color.black, 1));
		mapPanel.setBackground(new Color(189, 229, 242));
		JScrollPane mapScroller = new JScrollPane(mapPanel);
		mapScroller.getVerticalScrollBar().setUnitIncrement(20);
		
		mapTabPanel.add(mapScroller);
		mapTabPanel.setIconAt(mapPanels.size(), levelIcon);
		mapTabPanel.setTitleAt(mapPanels.size(), mapPanel.getMap().getMapName());
		mapPanels.add(mapPanel);
		mapIndex++;
		return mapPanel;
	}
	
	public void openMap() {
		File mapFile = null;
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setAcceptAllFileFilterUsed(false);
		while (true) {
			int operation = fileChooser.showSaveDialog(null);
			if (operation == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				if (file.isDirectory()) {
					mapFile = file;
					break;
				}
				System.out.println("You need to choose a directory!");
			} else if (operation == JFileChooser.CANCEL_OPTION)
				return;
		}
		MapPanel panel = newMap(mapFile);
		panel.getMap().load();
		mapTabPanel.setSelectedIndex(mapPanels.size() - 1);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setFont(new Font("Terminal", Font.BOLD, 12));
		g.drawString("Layer Alpha", this.getWidth() - 450, 60);
	}
	
	public ScriptManager getScriptManager() {
		return scriptManager;
	}
	
	public JTabbedPane getMapTabs() {
		return this.mapTabPanel;
	}
	
	public List<MapPanel> getMapPanels() {
		return this.mapPanels;
	}
	
}
