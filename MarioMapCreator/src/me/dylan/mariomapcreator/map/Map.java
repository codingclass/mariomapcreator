package me.dylan.mariomapcreator.map;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintWriter;

import javax.swing.JFileChooser;

import me.dylan.mariomapcreator.gui.MapCreatorPanel;
import me.dylan.mariomapcreator.gui.MapPanel;

public class Map {
	
	private File file;
	private int[][] foreground;
	private int[][] background;
	private int width;
	private int height;
	private int tileWidth;
	private int tileHeight;
	private int bgTile;
	private MapLayer currentLayer;
	private MapCreatorPanel mcp;
	private MapPanel mapPanel;
	
	public Map(MapCreatorPanel mcp, MapPanel mapPanel, File file) { //actually a folder
		this.mcp = mcp;
		this.mapPanel = mapPanel;
		this.file = file;
	}
	
	public Map(MapCreatorPanel mcp, MapPanel mapPanel, File file, int width, int height, int bgTile) {
		this(mcp, mapPanel, file);
		this.foreground = new int[width][height]; //Foreground size
		this.background = new int[width][height]; //Background size
		this.bgTile = bgTile;
		this.width = width * 16; //Width of map in pixel
		this.height = height * 16; //Height of map in pixels
		this.tileWidth = width;
		this.tileHeight = height;
		for (int x = 0; x < tileWidth; x ++) {
			for (int y = 0; y < tileHeight; y ++) {
				foreground[x][y] = bgTile;
				background[x][y] = bgTile;
			}
		} //generate blank background
		currentLayer = MapLayer.FOREGROUND;
	}
	
	public void load() {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(file.getPath() + "/leveldat.smm"))); 
			String line = "";
			MapLayer readingLayer = null;
			int h = 0;
			while ((line = reader.readLine()) != null) {
				if (line.startsWith("BGTILE ")) {
					bgTile = Integer.parseInt(line.split(" ")[1]);
				} else if (line.startsWith("WIDTH ")) { //todo update panel width
					width = Integer.parseInt(line.split(" ")[1]);
					line = reader.readLine();
					height = Integer.parseInt(line.split(" ")[1]);
					foreground = new int[width][height];
					background = new int[width][height];
				} else if (line.equalsIgnoreCase("FOREGROUND")) { //begin foreground
					readingLayer = MapLayer.FOREGROUND;
				} else if (line.equalsIgnoreCase("BACKGROUND")) {
					readingLayer = MapLayer.BACKGROUND;
					h = 0;
				} else {
					String tiles[] = line.split(" ");
					if (readingLayer == MapLayer.FOREGROUND) {
						for (int x = 0; x < width; x ++) {
							foreground[x][h] = Integer.parseInt(tiles[x]);
						}
						h ++;
					} else if (readingLayer == MapLayer.BACKGROUND) {
						for (int x = 0; x < width; x ++) {
							background[x][h] = Integer.parseInt(tiles[x]);
						}
						h ++;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void save() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setAcceptAllFileFilterUsed(false);
		while (true) {
			int operation = fileChooser.showSaveDialog(null);
			if (operation == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				if (file.isDirectory()) {
					this.file = file;
					break;
				}
				System.out.println("You need to choose a directory!");
			} else if (operation == JFileChooser.CANCEL_OPTION)
				return;
		}
		File scripts = new File(file.getPath() + "/scripts");
		if (!scripts.exists())
			scripts.mkdir();
		File tiledat = new File(file.getPath() + "/leveldat.smm");
		System.out.println("Saving tiles to " + tiledat.getPath());
		try {
			PrintWriter writer = new PrintWriter(new FileOutputStream(tiledat));
			writer.println("BGTILE " + getBackgroundTile());
			writer.println("WIDTH " + getTileWidth());
			writer.println("HEIGHT " + getTileHeight());
			writer.println("FOREGROUND");
			String tileLine = "";
			for (int y = 0; y < tileHeight; y ++) {
				for (int x = 0; x < tileWidth; x ++) {
					tileLine += foreground[x][y] + " ";
					if (x == tileWidth - 1) {
						writer.println(tileLine);
						tileLine = "";
					}
				}
			}
			writer.println("BACKGROUND");
			for (int y = 0; y < tileHeight; y ++) {
				for (int x = 0; x < tileWidth; x ++) {
					tileLine += background[x][y] + " ";
					if (x == tileWidth - 1) {
						writer.println(tileLine);
						tileLine = "";
					}
				}
			}
			writer.close();
			int index = mcp.getMapPanels().indexOf(mapPanel);
			mcp.getMapTabs().setTitleAt(index, file.getName());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		}
		System.out.println(file.getName() + " has been saved!");
	}
	
	public int[][] getForeground() {
		return foreground;
	}
	
	public int[][] getBackground() {
		return background;
	}
	
	public void setCurrentLayer(MapLayer mapLayer) {
		this.currentLayer = mapLayer;
	}
	
	public MapLayer getCurrentLayer() {
		return currentLayer;
	}
	
	public int getTileWidth() {
		return tileWidth;
	}
	
	public int getTileHeight() {
		return tileHeight;
	}
	
	public int getBackgroundTile() {
		return bgTile;
	}
	
	public String getMapName() {
		return file.getName();
	}
	
	public enum MapLayer {
		FOREGROUND, BACKGROUND;
	}
	
}
