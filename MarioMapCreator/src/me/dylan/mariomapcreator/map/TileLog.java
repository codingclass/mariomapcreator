package me.dylan.mariomapcreator.map;

public class TileLog {
	
	private int x;
	private int y;
	private int lastTile;
	private int newTile;
	
	public TileLog(int x, int y, int lastTile, int newTile) {
		this.x = x;
		this.y = y;
		this.lastTile = lastTile;
		this.newTile = newTile;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getLastTile() {
		return lastTile;
	}
	
	public int getNewTile() {
		return newTile;
	}
	
}
