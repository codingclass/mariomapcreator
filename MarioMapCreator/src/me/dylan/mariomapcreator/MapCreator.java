package me.dylan.mariomapcreator;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import me.dylan.mariomapcreator.gui.MapCreatorPanel;

public class MapCreator {
	
	public final static Dimension RESELOUTION = new Dimension(1280, 1280 / 14 * 10);
	
	public static void main(String[] args) {
		BufferedImage appIcon = null;
		try {
			appIcon = ImageIO.read(MapCreator.class.getResource("/appicon.png"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		JFrame frame = new JFrame("Mario Map Creator");
		frame.setSize(RESELOUTION);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setIconImage(appIcon);
		
		MapCreatorPanel panel = new MapCreatorPanel();
		panel.setSize(RESELOUTION);
		panel.init();
		
		frame.getContentPane().add(panel);
		frame.addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosed(WindowEvent e) {
				super.windowClosed(e);
				System.exit(0);
			}
			
		});
		frame.setVisible(true);
		
	}
	
}
