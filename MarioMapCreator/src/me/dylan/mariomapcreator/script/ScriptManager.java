package me.dylan.mariomapcreator.script;

import java.io.File;
import java.util.ArrayList;

public class ScriptManager {
	
	private ArrayList<Script> scripts = new ArrayList<Script>();
	//private LuaValue _G;
	
	public ScriptManager() {
		//_G = JsePlatform.standardGlobals();
		File scriptsFolder = new File("scripts/");
		for (File file : scriptsFolder.listFiles()) {
			Script script = new Script(this, file);
			scripts.add(script);
		}
	}
	
	public void registerScript(Script script) {
		scripts.add(script);
	}
	
	public Script getScript(int index) {
		return scripts.get(index);
	}
	
	public ArrayList<Script> getScripts() {
		return scripts;
	}
	
	/*public LuaValue getGlobals() {
		return _G;
	}*/
	
}
